package Modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class dbAnfitrion extends dbManejador implements dbPersistencia {

    @Override
    public void insertar(Object objeto) throws Exception {
        Anfitrion ven = (Anfitrion) objeto;

        String consulta = "INSERT INTO sistemas.anfitrion( numAf,nombre, evento, fecha, email, status) VALUES ( ?, ?, ?, ?, ?, ?);";

        if (this.conectar()) {
            try {
                PreparedStatement statement = conexion.prepareStatement(consulta);
                statement.setInt(1, ven.getNumAf()); // Assuming numAf is a string
                statement.setString(2, ven.getNombre());
                int eventoValue = (ven.getEvento()); // Assuming getEvento returns a string
                statement.setInt(3, eventoValue);
                String fechaString = String.format("%04d-%02d-%02d", ven.getAño(), ven.getMes(), ven.getDia());
                statement.setString(4, fechaString);
                statement.setString(5, ven.getEmail());
                statement.setInt(6, 0);
                statement.executeUpdate();
                JOptionPane.showMessageDialog(null, "Se insertó correctamente");
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Surgió un error al insertar: " + e.getMessage());
            } finally {
                this.desconectar(); // Make sure to close the connection
            }
        } else {
            JOptionPane.showMessageDialog(null, "No fue posible conectarse");
        }
    }

   @Override
public void actualizar(Object objeto) throws Exception {
    Anfitrion anfitrion = (Anfitrion) objeto;
    String consulta = "UPDATE sistemas.anfitrion SET numAf = ?, nombre = ?, evento = ?, fecha = ?, email = ?, status = ? WHERE id = ?";
    
    if (this.conectar()) {
        try {
            PreparedStatement statement = conexion.prepareStatement(consulta);
            statement.setInt(1, anfitrion.getNumAf());
            statement.setString(2, anfitrion.getNombre());
            int eventoValue = (anfitrion.getEvento());
            statement.setInt(3, eventoValue);
            String fechaString = String.format("%04d-%02d-%02d", anfitrion.getAño(), anfitrion.getMes(), anfitrion.getDia());
            statement.setString(4, fechaString);
            statement.setString(5, anfitrion.getEmail());
            statement.setInt(6, anfitrion.getStatus());
            statement.setInt(7, anfitrion.getId());
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Se actualizó correctamente");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Surgió un error al actualizar: " + e.getMessage());
        } finally {
            this.desconectar(); 
        }
    }
}


    @Override
public Object buscar(String codigo) throws Exception {
    Anfitrion anfitrion = new Anfitrion();
    
    if (this.conectar()) {
        try {
            String consultar = "SELECT * FROM sistemas.anfitrion WHERE numAf = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consultar);
            this.sqlConsulta.setString(1, codigo);
            this.Registros = this.sqlConsulta.executeQuery();
            
            if (this.Registros.next()) {
                anfitrion.setId(this.Registros.getInt("id"));
                anfitrion.setNumAf(this.Registros.getInt("numAf"));
                anfitrion.setNombre(this.Registros.getString("nombre"));
                anfitrion.setEvento(this.Registros.getInt("evento"));
                
                // Parse the date parts from the 'fecha' column
                String fechaString = this.Registros.getString("fecha");
                String[] fechaParts = fechaString.split("-");
                int año = Integer.parseInt(fechaParts[0]);
                int mes = Integer.parseInt(fechaParts[1]);
                int dia = Integer.parseInt(fechaParts[2]);
                anfitrion.setAño(año);
                anfitrion.setMes(mes);
                anfitrion.setDia(dia);
                
                anfitrion.setEmail(this.Registros.getString("email"));
                anfitrion.setStatus(this.Registros.getInt("status"));
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Surgió un error al buscar: " + e.getMessage());
        } finally {
            this.desconectar(); // Make sure to close the connection
        }
    }
    
    return anfitrion;
}

    @Override
public void deshabilitar(Object objecto) throws Exception {
    if (objecto instanceof Anfitrion) {
        Anfitrion anfitrion = (Anfitrion) objecto;

        String consulta = "UPDATE sistemas.anfitrion SET status = 1 WHERE numAf = ?";

        if (this.conectar()) {
            try {
                PreparedStatement statement = conexion.prepareStatement(consulta);
                ; // Set status to 1 (deshabilitado)
                statement.setInt(1, anfitrion.getNumAf());
                statement.executeUpdate();
                JOptionPane.showMessageDialog(null, "Se deshabilitó correctamente");
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, "Surgió un error al deshabilitar: " + e.getMessage());
            } finally {
                this.desconectar(); 
            }
        }
    } else {
        JOptionPane.showMessageDialog(null, "Objeto no válido para deshabilitar");
    }
}

    
}
