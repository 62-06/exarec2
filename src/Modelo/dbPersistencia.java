
package Modelo;

import javax.swing.table.DefaultTableModel;

public interface dbPersistencia {
    public void insertar(Object objecto) throws Exception;
    public void actualizar(Object objecto) throws Exception;
    public Object buscar(String codigo)throws Exception; 
    public void deshabilitar(Object objecto)throws Exception;
}
