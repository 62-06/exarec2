
package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Vista.dlg_Anfrition;
import Modelo.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class Controlador implements ActionListener {
    dlg_Anfrition vista;
    Anfitrion anf;
    dbAnfitrion db;
    boolean hab;

    public Controlador(dlg_Anfrition vista, Anfitrion anf, dbAnfitrion db) {
        this.vista = vista;
        this.anf = anf;
        this.db = db;
        
        vista.btnBuscar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnDesahabilitar.addActionListener(this);
        vista.cmbEventos.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        
    }

    private void iniciarVista() {
        try {
            deshabilitar();
            vista.setSize(582, 523);
            vista.setVisible(true);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vista, "Error al iniciar la vista: " + ex.getMessage());
 
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo){
            hab = true;
            vista.txtNum.setEnabled(hab);
            vista.txtNombre.setEnabled(hab);
            vista.cmbEventos.setEnabled(hab);
            vista.txtDía.setEnabled(hab);
            vista.txtMes.setEnabled(hab);
            vista.txtAño.setEnabled(hab);
            vista.txtEmail.setEnabled(hab);
            
            vista.btnGuardar.setEnabled(hab);
            vista.btnBuscar.setEnabled(hab);
            vista.btnDesahabilitar.setEnabled(hab);
            vista.btnLimpiar.setEnabled(hab);
            vista.btnCancelar.setEnabled(hab);
            
            vista.cmbEventos.setEnabled(hab);
        }
        else if (e.getSource() == vista.btnGuardar){
            if(ValiEmpty()!=true){
                JOptionPane.showMessageDialog(vista, "No deje ningun espacio vacio");
            }
            else{
                try {
                    anf = (Anfitrion)db.buscar(vista.txtNum.getText());
                    
                    anf.setNumAf(Integer.parseInt(vista.txtNum.getText()));
                    anf.setNombre(vista.txtNombre.getText());
                    anf.setEvento(vista.cmbEventos.getSelectedIndex());
                    anf.setDia(Integer.parseInt(vista.txtDía.getText()));
                    anf.setMes(Integer.parseInt(vista.txtMes.getText()));
                    anf.setAño(Integer.parseInt(vista.txtAño.getText()));
                    anf.setEmail(vista.txtEmail.getText());
                    db.insertar(anf);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Ocurrio un error al guardar: "+ex.getMessage());
                }
                
                
            }   
        }
        else if (e.getSource() == vista.btnBuscar){
            if(ValiBusc()!=true){
                JOptionPane.showMessageDialog(vista, "No deje ningun espacio vacio");
            }
            else{
                try {
                    anf = (Anfitrion)db.buscar(vista.txtNum.getText());
                    vista.txtNum.setText(String.valueOf(anf.getNumAf()));
                    vista.txtNombre.setText(anf.getNombre());
                    cmb();
                    vista.txtDía.setText(String.valueOf(anf.getDia()));
                    vista.txtMes.setText(String.valueOf(anf.getMes()));
                    vista.txtAño.setText(String.valueOf(anf.getAño()));
                    vista.txtEmail.setText(anf.getEmail());
                } catch (Exception ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        }
        
        
        else if (e.getSource() == vista.btnDesahabilitar){
            try {
                db.deshabilitar(vista.txtNum.getText());
            } catch (Exception ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        else if (e.getSource() == vista.btnCancelar){
            hab = false;
            vista.txtNum.setEnabled(hab);
            vista.txtNombre.setEnabled(hab);
            vista.cmbEventos.setEnabled(hab);
            vista.txtDía.setEnabled(hab);
            vista.txtMes.setEnabled(hab);
            vista.txtAño.setEnabled(hab);
            vista.txtEmail.setEnabled(hab);
            
            vista.btnGuardar.setEnabled(hab);
            vista.btnBuscar.setEnabled(hab);
            vista.btnDesahabilitar.setEnabled(hab);
            vista.btnLimpiar.setEnabled(hab);
            vista.btnCancelar.setEnabled(hab);
      
            vista.cmbEventos.setEnabled(hab);
        }
        else if (e.getSource() == vista.btnLimpiar){
            vista.txtNum.setText("");
            vista.txtNombre.setText("");
            vista.cmbEventos.setSelectedIndex(0);
            vista.txtDía.setText("");
            vista.txtMes.setText("");
            vista.txtAño.setText("");
            vista.txtEmail.setText("");
        }
        
        else if (e.getSource() == vista.btnCerrar){
            if (JOptionPane.showConfirmDialog(vista, "¿Desea cerrar?", "Cerrar", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                vista.setVisible(false);
                vista.dispose();
                System.exit(0);
            }

        }
    }
    
    public void cmb(){
        if(anf.getEvento()==1){
            vista.cmbEventos.setSelectedIndex(1);
        }
        else if(anf.getEvento()==2){
             vista.cmbEventos.setSelectedIndex(2);
        }
        else if(anf.getEvento()==3){
             vista.cmbEventos.setSelectedIndex(3);
        }
        else if(anf.getEvento()==4){
             vista.cmbEventos.setSelectedIndex(4);
        }
        else if(anf.getEvento()==5){
             vista.cmbEventos.setSelectedIndex(5);
        }
        else{
             vista.cmbEventos.setSelectedIndex(0);
        }
    }
    
    public boolean ValiEmpty(){
        if(!vista.txtNum.getText().isEmpty()
                && !vista.txtNombre.getText().isEmpty()
                && vista.cmbEventos.getSelectedIndex()!=0 
                && !vista.txtDía.getText().isEmpty()
                && !vista.txtMes.getText().isEmpty()
                && !vista.txtAño.getText().isEmpty()
                && !vista.txtEmail.getText().isEmpty()){
            return true;
        }
        else{
            return false; 
        }
        
    }
    
    public boolean ValiBusc(){
        if(!vista.txtNum.getText().isEmpty()){
            return true;
        }
        else{
            return false; 
        }
        
    }
    
    public void deshabilitar(){
        hab = false;
            vista.txtNum.setEnabled(hab);
            vista.txtNombre.setEnabled(hab);
            vista.cmbEventos.setEnabled(hab);
            vista.txtDía.setEnabled(hab);
            vista.txtMes.setEnabled(hab);
            vista.txtAño.setEnabled(hab);
            vista.txtEmail.setEnabled(hab);
            
            vista.btnGuardar.setEnabled(hab);
            vista.btnBuscar.setEnabled(hab);
            vista.btnDesahabilitar.setEnabled(hab);
            vista.btnLimpiar.setEnabled(hab);
            vista.btnCancelar.setEnabled(hab);
            
            vista.cmbEventos.setEnabled(hab);
    }
    
    
    public static void main(String[] args) {
        Anfitrion anf = new Anfitrion();
        dbAnfitrion db = new dbAnfitrion();
        dlg_Anfrition vista = new dlg_Anfrition();
        Controlador contr = new Controlador(vista, anf, db);
        contr.iniciarVista();
        
    }

    
    
}
